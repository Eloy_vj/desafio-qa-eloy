Feature: Buscar Contatos para iniciar conversa 

Para iniciar uma conversa com um contato existente, o usu�rio dever� pesquis�-lo
na guia de contato e depois selecion�-lo.

Scenario: Pesquisar um contato existente para iniciar uma conversa 
	Given Usuario est� na tela inicial do Whatsapp
	When Usuario clica na guia Contatos
	And Clicar no �cone pesquisar
	And Digitar o "nome" do contato existente no campo Pesquisar
	And Clicar no contato listado na tela	 
	Then A tela de conversa sera exibida
	And O cursor estara posicionado no campo Digite Aqui 