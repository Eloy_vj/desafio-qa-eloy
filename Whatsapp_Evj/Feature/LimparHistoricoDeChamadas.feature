Feature: Limpar Hist�rico de chamadas

Para apagar o hist�rico de chamadas realizadas, o usu�rio dever� acessar
a guia de chamadas, abrir o menu, selecionar a op��o de apagar e confirmar
a a��o posteriormente. Ap�s confirma��o, todas as chamadas do hist�rico
devem ser deletadas.

Scenario: Apagar historico de chamadas 
	Given Usuario esta na tela inicial do Whatsapp
	When Usuario clica na guia Contatos
	And Clicar no menu tres pontinhos
	And Selecionar a opcao Limpar Chamadas
	And Confirmar a acao de Limpar Chamadas	 
	Then Todas os registros do historico de chamadas serao deletados
	And A mensagem "Para ligar para os contatos que possuem Whatsapp, toque em no topo da sua tela." sera exibida