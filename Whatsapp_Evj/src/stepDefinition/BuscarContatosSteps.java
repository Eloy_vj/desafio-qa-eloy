package stepDefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BuscarContatosSteps {
		@Given("^Usuario est? na tela inicial do Whatsapp$")
		public void usuario_est_na_tela_inicial_do_Whatsapp() throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

		@When("^Usuario clica na guia Contatos$")
		public void usuario_clica_na_guia_Contatos() throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

		@When("^Clicar no ?cone pesquisar$")
		public void clicar_no_cone_pesquisar() throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

		@When("^Digitar o \"([^\"]*)\" do contato existente no campo Pesquisar$")
		public void digitar_o_do_contato_existente_no_campo_Pesquisar(String arg1) throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

		@When("^Clicar no contato listado na tela$")
		public void clicar_no_contato_listado_na_tela() throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

		@Then("^A tela de conversa sera exibida$")
		public void a_tela_de_conversa_sera_exibida() throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

		@Then("^O cursor estara posicionado no campo Digite Aqui$")
		public void o_cursor_estara_posicionado_no_campo_Digite_Aqui() throws Throwable {
		    // Write code here that turns the phrase above into concrete actions
		    throw new PendingException();
		}

}
