package br.com.checkout;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

public class Checkout {

	HashMap<String, Integer> mapRegras = new HashMap<String, Integer>();	


	public Checkout(HashMap<String, Integer> map)
	{
		this.mapRegras = map;		
	}		

	Integer qtdItem_A=0;
	Integer qtdItem_B=0;
	Integer qtdItem_C=0;
	Integer qtdItem_D=0;

	Integer total_A=0;
	Integer total_B=0;
	Integer total_C=0;
	Integer total_D=0;


	Integer valorUnitario_A = mapRegras.get("item_a_preco_unitario");
	Integer conjunto_A = mapRegras.get("item_a_conjunto");
	Integer valorEspecial_A = mapRegras.get("item_a_preco_especial");

	Integer valorUnitario_B = mapRegras.get("item_b_preco_unitario");
	Integer conjunto_B = mapRegras.get("item_b_conjunto");
	Integer valorEspecial_B =mapRegras.get("item_b_preco_especial");

	Integer valorUnitario_C = mapRegras.get("item_c_preco_unitario");
	Integer conjunto_C = mapRegras.get("item_c_conjunto");
	Integer valorEspecial_C = mapRegras.get("item_c_preco_especial");

	Integer valorUnitario_D = mapRegras.get("item_d_preco_unitario");
	Integer conjunto_D = mapRegras.get("item_d_conjunto");
	Integer valorEspecial_D	= mapRegras.get("item_d_preco_especial");	

	public void scan(String item)
	{
		//if(item.toUpperCase().equals(mapRegras.get("item_a_descricao").toString()))
		if(item.toUpperCase().equals("A"))
			qtdItem_A++;
		//if(item.toUpperCase().equals(mapRegras.get("item_b_descricao").toString()))
		if(item.toUpperCase().equals("B"))
			qtdItem_B++;
		//if(item.toUpperCase().equals(mapRegras.get("item_c_descricao").toString()))
		if(item.toUpperCase().equals("C"))
			qtdItem_C++;
		//if(item.toUpperCase().equals(mapRegras.get("item_d_descricao").toString()))
		if(item.toUpperCase().equals("D"))
			qtdItem_D++;
	}

	public int total()
	{
		//Contabiliza item A
		if(qtdItem_A > 0)
		{
			if(qtdItem_A < conjunto_A)
			{	
				total_A += qtdItem_A * valorUnitario_A;
			}			
			else if(qtdItem_A >= conjunto_A)
			{
				if((qtdItem_A % conjunto_A) == 0)
				{
					total_A += (qtdItem_A / conjunto_A) * valorEspecial_A;
				}
				else
				{
					Integer a_precoNormal = (qtdItem_A % conjunto_A) * valorUnitario_A;
					Integer a_precoConjunto = (qtdItem_A / conjunto_A) * valorEspecial_A;
					total_A += a_precoNormal + a_precoConjunto;
				}
			}

		}
		//Contabiliza item B
		if(qtdItem_B > 0)
		{
			if(qtdItem_B < conjunto_B)
			{	
				total_B += qtdItem_B * valorUnitario_B;
			}			
			else if(qtdItem_B >= conjunto_B)
			{
				if((qtdItem_B % conjunto_B) == 0)
				{
					total_B += (qtdItem_B / conjunto_B) * valorEspecial_B;
				}
				else
				{
					Integer b_precoNormal = (qtdItem_B % conjunto_B) * valorUnitario_B;
					Integer b_precoConjunto = (qtdItem_B / conjunto_B) * valorEspecial_B;
					total_B += b_precoNormal + b_precoConjunto;
				}
			}

		}
		//Contabiliza item C
		if(qtdItem_C > 0)
		{
			if(qtdItem_C < conjunto_C)
			{	
				total_C += qtdItem_C * valorUnitario_C;
			}			
			else if(qtdItem_C >= conjunto_C)
			{
				if((qtdItem_C % conjunto_C) == 0)
				{
					total_C += (qtdItem_C / conjunto_C) * valorEspecial_C;
				}
				else
				{
					Integer c_precoNormal = (qtdItem_C % conjunto_C) * valorUnitario_C;
					Integer c_precoConjunto = (qtdItem_C / conjunto_C) * valorEspecial_C;
					total_C += c_precoNormal + c_precoConjunto;
				}
			}

		}
		if(qtdItem_D > 0)
		{
			if(qtdItem_D < conjunto_D)
			{	
				total_D += qtdItem_D * valorUnitario_D;
			}			
			else if(qtdItem_D >= conjunto_D)
			{
				if((qtdItem_D % conjunto_D) == 0)
				{
					total_D += (qtdItem_D / conjunto_D) * valorEspecial_D;
				}
				else
				{
					Integer d_precoNormal = (qtdItem_D % conjunto_D) * valorUnitario_D;
					Integer d_precoConjunto = (qtdItem_D / conjunto_D) * valorEspecial_D;
					total_D += d_precoNormal + d_precoConjunto;
				}
			}

		}

		return total_A + total_B + total_C + total_D;
	}

}
