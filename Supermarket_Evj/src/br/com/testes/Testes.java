package br.com.testes;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.io.File;

import br.com.checkout.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;


public class Testes extends TestCase {
		 
	HashMap<String, Integer> map = new HashMap<String, Integer>();
	
	public void setUp(){		
		map.put("item_a_preco_unitario", 50);
		map.put("item_a_conjunto", 3);
		map.put("item_a_preco_especial", 130);
		map.put("item_b_preco_unitario", 30);
		map.put("item_b_conjunto", 2);
		map.put("item_b_preco_especial", 45);		
		map.put("item_c_preco_unitario", 40);
		map.put("item_c_conjunto", 2);
		map.put("item_c_preco_especial", 35);		
		map.put("item_d_preco_unitario", 60);
		map.put("item_d_conjunto", 3);
		map.put("item_d_preco_especial", 19);	
	}
    	
	
	public void testeTotal()
	{		
		assertEquals(0, price(""));
		assertEquals(50, price("A"));
		assertEquals(80, price("A,B"));
		assertEquals(115, price("C,D,B,A"));
		
		assertEquals(100, price("A,A"));
		assertEquals(130, price("A,A,A"));
		assertEquals(180, price("A,A,A,A"));
		assertEquals(230, price("A,A,A,A,A"));
		
		assertEquals(160, price("A,A,A,B"));
		assertEquals(175, price("A,A,A,B,B"));
		assertEquals(190, price("A,A,A,B,B,D"));
		assertEquals(190, price("D,A,B,A,B,A"));		
	}
	
	public void testeIncremental()
	{
		Checkout co = new Checkout(map);
		assertEquals( 0, co.total());
		        co.scan("A"); assertEquals( 50, co.total());
		        co.scan("B"); assertEquals( 80, co.total());
		        co.scan("A"); assertEquals(130, co.total());
		        co.scan("A"); assertEquals(160, co.total());
		        co.scan("B"); assertEquals(175, co.total());
	}
	
	int price(String goods)
	{
		Checkout co = new Checkout(map);		
		String itens[] = goods.split(",");
		for(int i = 0; i < itens.length; i++)
		{
			co.scan(itens[i]);
		}
		return co.total();
	}
	
	

}
